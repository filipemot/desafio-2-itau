import { Categoria } from './../shared/models/categorias.model';
import { CategoriasService } from './../shared/services/categorias.service';
import { Component, OnInit } from '@angular/core';
import { LancamentosService } from '../shared/services/lancamentos.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { Lancamentos } from '../shared/models/lancamentos.model';
import { Mes } from 'src/shared/models/mes.model';
import { IMes } from '../shared/models/mes.model';
import { UtilService } from 'src/shared/services/util.service';
import { Consolidados } from 'src/shared/models/consolidado.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'desafio2';

  listCategory: Categoria[];
  listReleases: Lancamentos[];

  listConsolidated: Consolidados[] = [];

  constructor(private categoriasService: CategoriasService,
    private lancamentosService: LancamentosService,
    private utilService: UtilService) { }

  ngOnInit(): void {
    this.categoriasService.getAllCategory().subscribe(categories => {
      this.listCategory = categories.body;

      this.lancamentosService.getAllReleases().subscribe(releases => {
        this.listReleases = releases.body;
        this.getConsolidated();
      });
    });
  }

  getConsolidated(): void {
    this.utilService.getMonths().forEach(item => {

      const listValues = this.listReleases.filter(x => x.mes_lancamento === item.id);

      let value = 0;
      listValues.forEach(release => value += release.valor);

      if (value > 0) {
        this.listConsolidated.push(new Consolidados(value, this.utilService.getMonth(item.id)));
      }

    });

  }

  getMonth(id: number): string {
    return this.utilService.getMonth(id);
  }

  getCategory(id: number): string {
    const category = this.listCategory.find(x => x.id === id);

    if (category) {
      return category.nome;
    }

    return '';
  }

}
