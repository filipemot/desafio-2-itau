export interface IConsolidados {
  valor?: number;
  mes?: string;
}

export class Consolidados implements IConsolidados {
  constructor(
    public valor?: number,
    public mes?: string
  ) { }
}
