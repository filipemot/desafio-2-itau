import { Injectable } from '@angular/core';
import { Mes } from '../models/mes.model';

@Injectable({ providedIn: 'root' })
export class UtilService {


  constructor() { }

  months = [
    new Mes(1, 'Janeiro'),
    new Mes(2, 'Fevereiro'),
    new Mes(3, 'Março'),
    new Mes(4, 'Abril'),
    new Mes(5, 'Maio'),
    new Mes(6, 'Junho'),
    new Mes(7, 'Julho'),
    new Mes(8, 'Agosto'),
    new Mes(9, 'Setembro'),
    new Mes(10, 'Outubro'),
    new Mes(11, 'Novembro'),
    new Mes(12, 'Dezembro')
  ];

  getMonths(): Mes[] {
    return this.months;
  }

  getMonth(id: number) {
    const month = this.getMonths().find(x => x.id === id);

    if (month) {
      return month.name;
    }

    return '';
  }

}
